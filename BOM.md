Bill Of Material (BOM)
======================

Bill Of Material (BOM) for build UltimateRedHeadTorch

For PCB:
--------

### PCB (x1):

See documentation about how to [build the PCB](./pcb/BUILD.md).

### Timer (x1):

LMC555 . (It will work with a Vcc of 3V. Normal LM555 needs at least 5V)

<https://www.ti.com/lit/ds/symlink/lmc555.pdf>

<https://es.aliexpress.com/item/4000041443031.html>

### Diode (x2):

1N4148

<https://es.aliexpress.com/item/4000928651264.html>

### Transistor (x2):

2N3904

<https://es.aliexpress.com/item/1005001697978901.html> (2N3904)

### Capacitor C1 (filter) (x1):

P5mm 10 nF (103)

### Capacitor C2 (oscillator) (x1):

P5mm 220 nF (224)

### Resistor R1 (x1):

1/4W 220 ohm (Red, Red, Brown)

### Resistor R2 (x1):

1/4W 2K2 ohm (Red, Red, Red)

### Resistor RL1 (x1):

1/4W 1K5 ohm (Brown, Green, Red)

### Resistor RL2 (x1):

1/4W 220 ohm (Red, Red, Brown)

### Resistor RB1 (x1):

1/4W 150K ohm (Brown, Green, Yellow)

### Resistor RB1 (x1):

1/4W 22K ohm (Red, Red, Orange)

Others electronic components:
-------------------------------

### LED (x3):

Red

<https://es.aliexpress.com/item/4000896062123.html>

  * Colour: 620-625nm (red)
  * Voltage gap: 2-2.2V
  * Nominal intensity: 20mA
  * Illumination angle:120 deg

Nevertheless the big illumination angle, these are the ones with more
homogeneous light beam.

### Potentiometer with button (x1):

Lineal 20K (B20K)

<https://es.aliexpress.com/item/33009473616.html>

### Mode (selection) switch (x1):

<https://es.aliexpress.com/item/32700941967.html>

### Power (on/off) switch (x1):

13.5mm

<https://es.aliexpress.com/item/32968438899.html>

### Battery holder (x1):

AAAx2

<https://es.aliexpress.com/item/33053795341.html>

### Wire

26AWG

<https://es.aliexpress.com/item/1005002138009919.html>

Colours:

 * Yellow
 * Green
 * Blue
 * Orange
 * Purple
 * Gray
 * Brown

Mechanical components:
----------------------

### LED holder (x3):

<https://es.aliexpress.com/item/32962646950.html>

### Screws M2x4 (x6):

<https://es.aliexpress.com/item/32975410255.html> -> *SEEMS THAT THIS PROVIDER IS SENDING WRONG SIZES*

<https://es.aliexpress.com/item/4000982209705.html>

### Screws M2.6x8 (x8):

<https://es.aliexpress.com/item/32975410255.html>

### Threaded rod (x1):

M3x59

### Nuts (x2):

M3

### Simple buckle (two crossbars) (x1):

20mm

<https://es.aliexpress.com/item/32819553590.html>

### composed buckle (three crossbars) (x1):

20mm

<https://es.aliexpress.com/item/32649861803.html>

### Elastic ribbon (about 1.20m)

20mm wide

<https://es.aliexpress.com/item/33057885200.html>

