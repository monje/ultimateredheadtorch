UltimateRedHeadTorch build guide
================================

## Step 1: PCB wiring

After having soldered all the components on the PCB, it's time to solder the
wires. Lets use next image as reference:

![PCB wiring 1](media/01-ultimateredheadtorch-pcb_wiring_1.jpg)

Here a numerated list of wires marked in the image:

 1. Brown wire of about 2.5cm from the "P2" hole to one of the terminals at
the end of the potentiometer.

 2. Gray wire of about 2cm from the "PV" hole to the middle terminal of the
potentiometer.

 3. Brown wire of about 1.5cm from the "P1" hole to the other terminal at the
end of the potentiometer.

 4. Cut about 3cm of red wire of the battery holder and solder this small
piece to the "J1+" hole.

 5. The black wire of the battery holder to the "J1-" hole. Before solder,
cut and discard about 2cm of it. In this maner the black wire of the battery
holder is not too long.

 6. Purple wire of about 4cm to the "L2" hole.

 7. Orange wire of about 4cm to the "L1" hole.

 8. Yellow wire of about 6cm to the "+" hole near "L1" one.

After finish the soldering, it's time to place the PCB and the potentiometer
into the main 3D printed box. It's a few tricky, so be patient. Note the
position of the potentiometer. Use the M2x4 screws to fix the PCB.

![PCB wiring 2](media/02-ultimateredheadtorch-pcb_wiring_2.jpg)

Note that the orange and purple wires are passed through the hole for the
selection switch in the main box. Also, the yellow wire should be pass through
the hole for the hinge in the main box.

## Step 2: selection switch wiring

Lets solder the selection switch as in the next image. Note the different
spacing between terminals and use then as reference.

![switch wiring 1](media/03-ultimateredheadtorch-switch_wiring_1.jpg)

 1. Green wire of about 8cm to one of the end terminal at one of the sides of
the switch.

 2. A little piece of green wire connecting the terminal of the point 1 to the
terminal at the other end in same side that before. Note the larger spacing of
this terminal.

 3. Blue wire of about 8cm to the second terminal of the other side. Use the
larger spacing of the preceding terminal for find correct orientation.

 4. A little piece of blue wire connecting the terminal of the preceding point to
the preceding terminal of same side.

Now it's time to solder the selection switch to the wires that come out the
main box (orange and purple):

![switch wiring 2](media/04-ultimateredheadtorch-switch_wiring_2.jpg)

Solder the orange wire to the terminal indicated by the arrow in preceding image.

![switch wiring 3](media/05-ultimateredheadtorch-switch_wiring_3.jpg)

Solder the purple wire to the terminal indicated by the arrow in preceding image.

## Step 3: seat the selection switch in place

![switch in place](media/06-ultimateredheadtorch-switch_wiring_4.jpg)

Seat the selection switch in place and fix it with 2 M2x4 screws, in the position
indicated in the preceding image.

## Step 4: Put LEDs in place

![leds in holder 1](media/07-ultimateredheadtorch-led_holders_1.jpg)

Gently place the LEDs into its holders. The fix is a few loose, but they will
be glued later.

![leds in holder 2](media/08-ultimateredheadtorch-led_holders_2.jpg)

Insert the LED holders (with the LEDs) in the holes of the 3D printed LED cap.
It can be a little difficult, do be patient.

![leds in holder 3](media/09-ultimateredheadtorch-led_holders_placement.jpg)

Play attention to orient the pins of the LEDs in the way showed in preceding image,
so solder the wires to the terminal of the LEDs will be easier. Make sure that they
are in perpendicular position.

## Step 5: Glue LEDs, on/off button and battery holder

Use a epoxy glue for fix in site the LEDs, on/off button and battery holder.

![leds glued](media/10-ultimateredheadtorch-leds_glued.jpg)

Note in preceding image how the LEDs are glued.

![onoff battery holder glued](media/11-ultimateredheadtorch-onoff_batholder_glued.jpg)

Glue the on/off button and the battery holder. Use some kind of nippers to keep
the battery holder in place. It's very recommend screw a "dummy" main box (the
highest part of the main box serrated) in order to fix the correct position of
the battery holder.

## Step 6: Solder the on/off button terminals

Once the glue is cured, it's time to wire the on/off button.

![onoff wiring](media/12-ultimateredheadtorch-onoff_wiring.jpg)

 1. Solder the red wire from the PCB to one of the terminals of the on/off
button, as showed in the image.

 2. Solder the red wire from the battery holder to the other terminal of the
on/off button, as showed in the image.

## Step 7: LEDs wiring

**IMPORTANT** Don't forget pass the yellow, green and blue wires throw the 3D
printed hinge in the right orientation.

![LEDs wiring 1](media/13-ultimateredheadtorch-leds_wiring_1.jpg)

Solder the yellow, green and blue wire in this manner:

 - Yellow wire to all the three positive terminals of the LEDs.

 - Green wire to the negative terminal of the middle LED.

 - Blue wire to both the negative terminals of the two side LEDs. Consider use
a little piece of blue wire to connect the negative terminals of both LEDs.
Then connect the main blue wire to them.

![LEDs wiring 2](media/14-ultimateredheadtorch-leds_wiring_2.jpg)

After finish the LEDs wiring, the set must look as in the preceding image.

## Step 8: Place the hinge

Pick up the excess wires and screw the 3D printed hinge piece to the 3D
printed LED cap.

![wire tunnel](media/15-ultimateredheadtorch-wires_tunnel.jpg)

This preceding image shows how the hinge must be paced.

![edge sanding](media/16-ultimateredheadtorch-edge_sanding.jpg)

Probably, will be necessary sand a few the edge of the 3D printed main box in
order to permit a right movement of the hinge, as showed.

![pivot placement](media/17-ultimateredheadtorch-pivot_placement.jpg)

Finally, insert the pivot for the hinge and fix it with a nut in each end.

## Step 9: Make the elastic ribbon head support

For make the elastic ribbon head support we will use 2 pieces of 20mm wide
elastic ribbon, of 17cm and 50cm and two buckles, one "simple" (two crossbars)
and one "composed" (three crossbars).

![buckle short elastic](media/18-ultimateredheadtorch-buckle_short_elastic.jpg)

Sew one of the end of the short ribbon of 17cm to the simple buckle, as showed
in the preceding image.

![buckle long elastic 1](media/19-ultimateredheadtorch-buckle_long_elastic_1.jpg)

Pass the long ribbon of 50cm ribbon through previously sewing simple buckle
and the composed buckle, as showed in the preceding image.


![buckle long elastic 2](media/20-ultimateredheadtorch-buckle_long_elastic_2.jpg)

Sew a knot in the long ribbon for avoid it to pull out the composed buckle, as
showed in the preceding image.

![elastic placement](media/21-ultimateredheadtorch-elastic_placement.jpg)

Finally, pass the free ends of the ribbons throw the grooves of the 3D printed
box cap and sew them.

