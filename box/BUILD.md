UltimateRedHeadTorch box build guide
====================================

## Instructions for 3D printing

I recommend use ABS for printing the pieces, given its greater mechanical
resistance and to high temperatures.

Here some relevant parameters I use in the cura slicer for printing these
pieces:

 - Wall thickness: 1mm

 - Fill gaps between walls: everywhere

 - Top/bottom thickness: 1mm

 - Infill density: 20%

 - Infill pattern: lines

 - Generate support: yes

 - Support structure: normal

 - Support placement: everywhere

 - Support pattern: lines

 - Support density: 30%

 - Build plate adhesion type: Brim

 - Brim width: 12mm

## Post-processing

After printing the different 3D box components, it's necessary a mechanical
post-processing in order to prepare them for use. This process is illustrated
in the next images.

![pivot](media/01-ultimateredheadtorch_box-pivot.jpg)

You will need a shaft made with a 3mm threaded rod. It must be exactly
59mm long.

![drill](media/02-ultimateredheadtorch_box-drill.jpg)

You will need three drills of diameter of 3mm, 2mm and 1.5mm. We will use
adhesive tape to mark the deep of the the 2mm drill until 7mm, and the 1.5mm
drill until 3mm.

![pcb holes](media/03-ultimateredheadtorch_box-pcb_holes.jpg)

Use the 1.5mm drill for finish the PCB support holes until a deep of 3mm,
as showed in the  preceding image.

![switch](media/04-ultimateredheadtorch_box-switch.jpg)

Use the 1.5mm drill for finish the screw holes for fix the selection switch,
as showed in the  preceding image.

![main](media/05-ultimateredheadtorch_box-main.jpg)

Use the 2mm drill for finish the main box holes until a deep of 7mm,
as showed in the  preceding image.

![main cap](media/06-ultimateredheadtorch_box-main_cap.jpg)

Use the 2mm drill for finish the screw holes for the main box cap,
as showed in the  preceding image.

![hinge](media/07-ultimateredheadtorch_box-hinge.jpg)

Use the 2mm drill for finish the screw holes for the hinge piece,
as showed in the  preceding image.

![led cap](media/08-ultimateredheadtorch_box-led_cap.jpg)

Use the 2mm drill for finish the LEDs cap holes until a deep of 7mm,
as showed in the  preceding image.

![shaft](media/09-ultimateredheadtorch_box-shaft.jpg)
Use the 2mm drill in a first step for moulding the shaft hole. Then, use
the 3mm drill for finish it, as showed in the  preceding image.

![hinge](media/10-ultimateredheadtorch_box-hinge.jpg)

Use the 2mm drill in a first step for moulding the hinge hole. Then, use
the 3mm drill for finish it, as showed in the  preceding image.

![sand](media/11-ultimateredheadtorch_box-sand.jpg)

Finally, you should use a file for a general finish of the printed pieces.
Play special attention to adjust the encase of the hinge piece in the piece
of the shaft hole. It should fit very tight, in order to permit the movement,
but with some resistance for maintain the position when mounted.

