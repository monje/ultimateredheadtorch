UltimateRedHeadTorch PCB build guide
====================================

I recommend order the PCB in any inline PCB factory. I ordered mine in
[jlcpcb.com](https://jlcpcb.com/).

When order you must upload the gerber files compressed in zip format. You can
find this compressed file [here](./gerber-2021-07.zip).

You should choose a PCB thickness of 1.2mm. You are free to choose other
parameters, as colour, as you wish.

